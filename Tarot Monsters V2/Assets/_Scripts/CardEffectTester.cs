﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CardEffectTester
/// </summary>
public class CardEffectTester : MonoBehaviour {
	#region Fields

	public TarotCard card;

	#endregion
	
	void Start() {
		if (!card.Activate()) {
			print("method not found");
		}
	}

	void Update() {
		
	}
}