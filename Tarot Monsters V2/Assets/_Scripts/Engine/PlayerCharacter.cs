﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerCharacter
/// </summary>
[CreateAssetMenu]
public class PlayerCharacter : ScriptableObject {
	#region Fields

	public string characterClass;
	public Sprite tokenSprite;
	public int str, agi, def, wis;

	#endregion


}