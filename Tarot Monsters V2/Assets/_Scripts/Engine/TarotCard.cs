﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TarotCard
/// </summary>
[CreateAssetMenu]
public class TarotCard : ScriptableObject {
	#region Fields

	public string displayName, description;
	public CardClass cardClass;
	public int count;
	public int coolDown;
	public int maxUses;

	#endregion

	public bool Activate() {
		Type effectType = typeof(CardEffects);
		MethodInfo theMethod = effectType.GetMethod(name);
		if (theMethod != null) {
			theMethod.Invoke(effectType, null);
			return true;
		} else {
			return false;
		}
	}

	public enum CardClass {
		Major,
		Minor
	}
	public enum PlayTime {
		Combat,
		NonCombat,
		Any
	}
	public enum ActivePassive {
		Active,
		Passive
	}
}