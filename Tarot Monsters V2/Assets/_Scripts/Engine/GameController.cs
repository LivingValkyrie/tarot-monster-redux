﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GameController
/// </summary>
public class GameController : MonoBehaviour {
	#region Fields

	public TurnPhase phase;
	public static GameController instance;

	public Queue<TarotCard> majorDeck, minordeck;
	public GameObject[] menus;
	public GameObject[] charSelect;

	#endregion

	void Awake() {
		if (instance == null) {
			instance = this;

			DontDestroyOnLoad(gameObject);
		} else {
			if (instance != this) {
				Destroy(gameObject);
			}
		}
	}

	void Start() {
		if (SceneManager.GetActiveScene().name == "Menu") {
		ChangeMenu(0);
			CharacterSelect(0);
		}

	}

	void Update() {
		switch (phase) {
			case TurnPhase.Start:

				break;
			case TurnPhase.Draw:

				break;
			case TurnPhase.Roll:

				break;
			case TurnPhase.Move:

				break;
			case TurnPhase.Combat:

				break;
			case TurnPhase.End:

				break;
		}
	}

	public void LoadScene(string scene) {
		if (!string.IsNullOrEmpty(scene)) {
			print("moving to " + scene);
			SceneManager.LoadScene(scene);
		} else {
			print(scene + " is empty or null");
		}
	}

	public void ChangeMenu(int next) {
		foreach (GameObject menu in menus) {
			menu.SetActive(false);
		}
		menus[next].SetActive(true);
	}

	public void CharacterSelect(int players) {
		activePlayers = players;
		for (int i = 0; i < charSelect.Length; i++) {
			charSelect[i].SetActive(i <= players);
		}
	}

	int activePlayers = 0;
	public Text error;
	public PlayerCharacter playerOneData, playerTwoData, playerThreeData, playerFourData;

	public void AssignCharacterOne(int id) {
		playerOneData = Resources.LoadAll<PlayerCharacter>("")[id];
				
	}

	public void AssignCharacterTwo(int id) {
		playerTwoData = Resources.LoadAll<PlayerCharacter>("")[id];

	}

	public void AssignCharacterthree(int id) {
		playerThreeData = Resources.LoadAll<PlayerCharacter>("")[id];

	}

	public void AssignCharacterFour(int id) {
		playerFourData = Resources.LoadAll<PlayerCharacter>("")[id];

	}

	public void StartGame() {
		for (int i = 0; i <= activePlayers; i++) {

			if (charSelect[i].activeInHierarchy) {
				if (!charSelect[i].GetComponentInChildren<CharacterDropdown>().Chosen) {
					error.text = string.Format("Player {0} needs to select a character!", i + 1);
					return;
				}
			}
		}

		error.text = "";
		SceneManager.LoadScene("Main");
	}

	public void QuitGame() {
		//todo add prompt?
		Application.Quit();
	}
}

public enum TurnPhase {

	Menu,
	Start,
	Draw,
	Roll,
	Move,
	Combat,
	End,
	GameOver

}