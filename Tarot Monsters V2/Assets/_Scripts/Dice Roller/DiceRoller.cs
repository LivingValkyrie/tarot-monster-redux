﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: DiceRoller
/// </summary>
public class DiceRoller : MonoBehaviour {
	#region Fields

	public Dice dice;
	public int toRoll;
	List<Dice> rollingdice;
	bool waiting = false;

	#endregion

	void Start() {
		rollingdice = new List<Dice>();
		RollDice(toRoll);
	}

	public void RollDice(int count) {
		for (int i = 0; i < count; i++) {
			print(rollingdice);
			rollingdice.Add(Instantiate(dice, transform.position + (Random.onUnitSphere * 2), Quaternion.identity));

		}
		waiting = true;
	}

	void FixedUpdate() {
		if (waiting) {
			foreach (Dice die in rollingdice) {
				if (!die.Done) {
					return;
				}
			}

			print("total is " + Total());
			waiting = false;
		}
	}

	int Total() {
		int x = 0;
		foreach (Dice die in rollingdice) {
			x += die.currentValue;
		}
		return x;
	}
}

public enum DiceType {
	D2,
	D3,
	D4,
	D6,
	D8,
	D10,
	D12,
	D20,
	D100
}