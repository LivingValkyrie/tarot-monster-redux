﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Dice
/// </summary>
public class Dice : MonoBehaviour {
	#region Fields

	public int currentValue = 0;
	public LayerMask dieValueLayerMask = -1;
	bool _isMoving = true;
	public bool Done {
		get { return !_isMoving; }
	}

	public DiceFace[] faces;
	public float force = 5;
	Rigidbody rb;

	#endregion

	void Start() {
		rb = GetComponent<Rigidbody>();
		for (int i = 0; i < faces.Length; i++) {
			var diceFace = faces[i];
			diceFace.faceValue = Random.Range(0, 100);
			diceFace.obj.GetComponentInChildren<Text>().text = diceFace.faceValue.ToString();
		}
		rb.AddForce(new Vector3(Random.Range(-1,1), Random.Range( -1, 1 ) , Random.Range( -1, 1 ) ) * force ,ForceMode.Impulse);
	}

	// Update is called once per frame
	void FixedUpdate() {
		if (rb.IsSleeping()) {
			_isMoving = false;
		}

		if (!_isMoving) {
			RaycastHit hitInfo;

			if (Physics.Raycast(transform.position, Vector3.up, out hitInfo, Mathf.Infinity, dieValueLayerMask)) {
				currentValue = Int32.Parse(hitInfo.collider.GetComponent<Text>().text);

				Debug.Log(this.ToString() + " " + currentValue);

				//send this roll to diceroller
			}
		}
	}

}

[Serializable]
public struct DiceFace {
	public GameObject obj;
	public int faceValue;
}

[CustomPropertyDrawer(typeof(DiceFace))]
public class IngredientDrawer : PropertyDrawer {
	// Draw the property inside the given rect
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		// Calculate rects
		var amountRect = new Rect(position.x, position.y, 100, position.height);
		var unitRect = new Rect(position.x + 105, position.y, 50, position.height);

		// Draw fields - passs GUIContent.none to each so they are drawn without labels
		EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("obj"), GUIContent.none);
		EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("faceValue"), GUIContent.none);

		// Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty();
	}
}