﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CharacterDropdown
/// </summary>
public class CharacterDropdown : MonoBehaviour {
	#region Fields

	public Image image;
	public Text strText, defText, agiText, wisText;
	public bool Chosen {get; private set; }
	
	#endregion
	
	void Start() {
		var dd = GetComponent<Dropdown>();
		PlayerCharacter[] pdata = Resources.LoadAll<PlayerCharacter>("");
		//print(pdata.Length);
		dd.options = new List<Dropdown.OptionData>();
		foreach (var pc in pdata) {
		Dropdown.OptionData test = new Dropdown.OptionData(pc.characterClass);
			dd.options.Add(test);
		}

		Chosen = false;
	}

	public void UpdateCharacter(int id) {
		var pc = Resources.LoadAll<PlayerCharacter>("")[id];
		image.sprite = pc.tokenSprite;

		strText.text = pc.str.ToString();
		defText.text = pc.def.ToString();
		agiText.text = pc.agi.ToString();
		wisText.text = pc.wis.ToString();

		Chosen = true;
	}
}